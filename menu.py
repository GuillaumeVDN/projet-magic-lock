# coding=utf-8

from sense_hat import ACTION_PRESSED, DIRECTION_MIDDLE, DIRECTION_LEFT, DIRECTION_RIGHT

import menu_options
from menu_options import RESULT_END_PROGRAM, RESULT_RESTART_PROGRAM, RESULT_SHOW_MENU
import design
import file
import time
import utils

# -------------------- Options du menu --------------------

option_quit = (menu_options.shutdown, design.BoutonPower)
option_restart = (menu_options.restart, design.BoutonRestart)
option_decode_message = (menu_options.decode_message, design.ClosedDoor)
option_show_message = (menu_options.show_message, design.OpenDoor)
option_delete_message = (menu_options.delete_message, design.Trash)
option_enter_message = (menu_options.enter_message, design.Parchemin)

# ---------------- Affichage du menu ----------------

def show_menu(sense, correct_password = None):
    # clear l'écran pendant 0.5 seconde
    sense.clear()
    time.sleep(0.5)
    # différentes possibilités en fonction des paramètres
    if file.has_file():
        if correct_password is not None:
            options = (option_show_message, option_delete_message, option_quit, option_restart)
        else:
            options = (option_decode_message, option_quit, option_restart)
    else:
        options = (option_enter_message, option_quit, option_restart)
    # log
    options_str = ''
    for option in options:
        if len(options_str) != 0: options_str += ', '
        options_str += str(option[0])
    utils.log('\tShow menu, options = ' + options_str)
    # afficher la première option disponible
    k = 0
    design.display(sense, 0, options[k][1])
    # l'utilisateur doit sélectionner un choix du menu à l'aide du joystick
    while True:
        event = sense.stick.wait_for_event()
        if event.action == ACTION_PRESSED:
            # l'utilisateur a cliqué à droite
            if event.direction == DIRECTION_RIGHT:
                # augmenter l'index de l'option
                k += 1
                if k > len(options) - 1:
                  k = 0
                # afficher la nouvelle option
                design.display(sense, 0, options[k][1])
                # log
                utils.log('\tClic à droite, nouvelle option ' + str(options[k][0]))
            # l'utilisateur a cliqué à gauche
            elif event.direction == DIRECTION_LEFT:
                # diminuer l'index de l'option
                k -= 1
                if k < 0:
                    k = len(options) - 1
                # afficher la nouvelle option
                design.display(sense, 0, options[k][1])
                # log
                utils.log('\tClic à gauche, nouvelle option ' + str(options[k][0]))
            # l'utilisateur a cliqué au milieu : sélectionner l'option
            elif event.direction == DIRECTION_MIDDLE:
                utils.log('\tClic au milieu, appel de l\'option ' + str(options[k][0]))
                # appel de la fonction
                option_function = options[k][0]
                result = option_function(sense, correct_password)
                # afficher le menu
                if result[0] == RESULT_SHOW_MENU:
                    param = None
                    if len(result) > 1: param = result[1]
                    return show_menu(sense, param)
                # retourner le résultat
                return result[0]
