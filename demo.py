# coding=utf-8

from sense_hat import SenseHat, ACTION_PRESSED
import time
import traceback

import challenges
import menu
import design
import utils
from menu_options import RESULT_RESTART_PROGRAM

# -------------------- Fonction principale --------------------

def main(sense):
    # on demande à l'utilisateur de passer les répreuves
    while True:
        utils.log('Main : début des épreuves')
        success = challenges.ask_challenges(sense)
        # il a réussi, afficher le menu et quitter la boucle
        if success:
            utils.log('Main : épreuves complètes, affichage du menu')
            result = menu.show_menu(sense)
            # Redémarrage
            if result == RESULT_RESTART_PROGRAM:
                # clear l'écran pendant 0.5 seconde
                sense.clear()
                time.sleep(0.5)
                # continue main
                continue
            # Fini
            return
        # il n'a pas réussi
        else:
            utils.log('Main : échec des épreuves, en attente d\'un event pour recommencer')
            # attente d'un clic du joystick pour réessayer
            design.display(sense, 0, design.Question)
            started = time.time()
            while True:
                # il a cliqué
                pressed = False
                for event in sense.stick.get_events():
                    if event.action == ACTION_PRESSED:
                        pressed = True
                        break
                if pressed:
                    break
                # aucun clic après 10 secondes, quitter le programme et éteindre le raspberry
                if time.time() - started > 10:
                    utils.log('Main : pas d\'event après 10 secondes, return')
                    return

# -------------------- Au démarrage du programme --------------------

# création du sense_hat
sense = SenseHat()
sense.low_light = True

# fonction principale
try:
    main(sense)
    sense.clear()
# erreur dans le programme
except:
    utils.log('\n\n\t\t\tUne erreur s\'est produite durant l\'exécution du programme :')
    utils.log(traceback.format_exc())
    traceback.print_exc()
    utils.log('\n\n')
    sense.clear()
