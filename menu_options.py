# coding=utf-8

import utils
import menu
import file
import user_input
import design

# Chaque option du menu est une fonction qu'on peut appeler
#  avec le paramètre sense (l'instance de SenseHat) et le
#  paramètre correct_password qui est None si l'utilisateur
#  n'a pas entré le bon mot de passe ou bien le mot de passe
#  s'il a entré le bon
# Chaque fonction d'option doit retourner un tuple (result, param)

# -------------------- Résultats --------------------

RESULT_END_PROGRAM = 'end'
RESULT_RESTART_PROGRAM = 'restart'
RESULT_SHOW_MENU = 'menu'

# -------------------- Option : quitter --------------------

def shutdown(sense, correct_password):
    utils.log('End program')
    return (RESULT_END_PROGRAM, None)

# -------------------- Option : redémarrer --------------------

def restart(sense, correct_password):
    utils.log('Restart program')
    return (RESULT_RESTART_PROGRAM, None)

# -------------------- Option : décoder le message  --------------------

def decode_message(sense, correct_password):
    # demander un mot de passe à l'utilisateur et tenter de décrypter
    utils.log('\t\tEn attente de mot de passe')
    key = user_input.ask_code(sense)
    message = file.decrypt_file(key)
    # mot de passe incorrect
    if message is None:
        utils.log('\t\tMot de passe incorrect, retour au menu')
        design.display(sense, 1, design.Erreur)
        return (RESULT_SHOW_MENU, None)
    # mot de passe correct
    else:
        utils.log('\t\tMot de passe correct, retour au menu avec correct_password')
        design.display(sense, 1, design.Succes)
        return (RESULT_SHOW_MENU, key)

# -------------------- Option : afficher le message --------------------

def show_message(sense, correct_password):
    # afficher le message
    message = file.decrypt_file(correct_password)
    utils.log('\t\tAffichage du message')
    sense.show_message(message, text_colour = (255, 127, 0))
    # retour au menu
    utils.log('\t\tRetour au menu avec correct_password')
    return (RESULT_SHOW_MENU, correct_password)

# -------------------- Option : supprimer le message --------------------

def delete_message(sense, correct_password):
    # supprimer le message
    utils.log('\t\tSuppression du message')
    file.delete_file()
    # retour au menu
    utils.log('\t\tRetour au menu')
    return (RESULT_SHOW_MENU, None)

# -------------------- Option : entrer un message --------------------

def enter_message(sense, correct_password):
    # demander un message à l'utilisateur
    utils.log('\t\tEn attente de message')
    design.display(sense, 0.5, design.Question)
    message = user_input.ask_text(sense)
    # message incorrect
    if message is None or len(message) == 0:
        # retour au menu
        utils.log('\t\tMessage invalide, retour au menu')
        return (RESULT_SHOW_MENU, None)
    # demander un mot de passe à l'utilisateur
    utils.log('\t\tMessage valide, en attente de mot de passe')
    design.display(sense, 0.5, design.Question)
    key = user_input.ask_code(sense)
    # mot de passe incorrect
    if key is None or len(key) == 0:
        # retour au menu
        utils.log('\t\tMot de passe invalide, retour au menu')
        return (RESULT_SHOW_MENU, None)
    # encrypter le message
    utils.log('\t\tMot de passe valide, cryptage du fichier')
    file.encrypt_file(message, key)
    # retour au menu
    utils.log('\t\tFichier crypté, retour au menu avec correct_password')
    return (RESULT_SHOW_MENU, key)
