# coding=utf-8

import random

import challenges_types
import design
import utils

# -------------------- Demander des challenges  --------------------

# types de challenges
challenges = [challenges_types.wake_up, challenges_types.sphinx, challenges_types.genie, challenges_types.compagnon, challenges_types.door]

def ask_challenges(sense):
    # Log
    challenges_str = ''
    for challenge in challenges:
        if len(challenges_str) != 0: challenges_str += ', '
        challenges_str += str(challenge)
    utils.log('\tDébut des épreuves, challenges = ' + challenges_str)
    # demander à l'utilisateur de faire les épreuves
    for challenge in challenges:
        utils.log('\tépreuve ' + str(challenge))
        # s'il n'a pas réussi le challenge...
        if not challenge(sense):
            # animation d'échec
            design.display(sense, 2, design.Erreur)
            # il n'a plus d'essaie
            utils.log('\téchec de l\'épreuve ' + str(challenge))
            return False
    # il a réussi toutes les épreuves
    return True
